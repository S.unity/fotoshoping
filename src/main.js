import { createApp } from 'vue'
import App from './App.vue'
import "./assets/main.css"
import "./assets/style.scss"
import router from './router';
import clickOutside from './utils/click-outside';
import store from './vuex/store';

import firebase from "firebase/compat";
/* code from our Firebase console */
let firebaseConfig = {
  apiKey: "AIzaSyBO6Bx0ZBsBPzjer98cMYwB8WxYlEA_ojU",
  authDomain: "fotoshopping.firebaseapp.com",
  databaseURL: "https://fotoshopping-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fotoshopping",
  storageBucket: "fotoshopping.appspot.com",
  messagingSenderId: "460467935324",
  appId: "1:460467935324:web:5ff6a00d0034249756ee99",
  measurementId: "G-0P8W5J3R79"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// запрет на вход не авторизованному пользователю
router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    /*
    этот путь требует авторизации, проверяем залогинен ли
    пользователь, и если нет, запрещаем доступ к странице
     */
    if (store.user && store.user.id) {
      next()
    } else {
      console.log('Пользователь не зарегестрирован')
      next({
        path: '/404'
      })
    }
  } else {
    next() // всегда так или иначе нужно вызвать next()!
  }
})

createApp(App)
  .use(store)
  .use(router)
  .directive('click-outside', clickOutside)
  .mount('#app')
