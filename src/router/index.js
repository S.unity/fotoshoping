import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

const isServer = typeof window === 'undefined';
const history = isServer ? createMemoryHistory() : createWebHistory();
const routes = [
    {
        path: '/',
        name: 'Index',
        component: ()=> import('../views/Index.vue'),
    },
    {
        path: '/shop/:category',
        name: 'Shop',
        component: ()=> import('../views/Shop.vue'),
        props: true,
    },
    {
        path: '/product/:category/:product/:id',
        name: 'Product',
        component: ()=> import('../views/Product.vue'),
        props: true,
    },
    {
        path: '/basket',
        name: 'Basket',
        component: ()=> import('../views/Basket.vue'),
    },
    {
        path: '/about',
        name: 'About',
        component: ()=> import('../views/About.vue'),
    },
    {
        path: '/contacts',
        name: 'Contacts',
        component: ()=> import('../views/Contacts.vue'),
    },
    // {
    //     path: '/',
    //     name: 'Delivery',
    //     component: defineAsyncComponent(()=> import('../views/Index.vue')),
    // },
    {
        path: '/profile',
        name: 'Profile',
        component: ()=> import('../views/Profile.vue'),
        meta: { requiresAuth: true },
    },
    {
        path: '/orders',
        name: 'Orders',
        component: ()=> import('../views/Orders.vue'),
        meta: { requiresAuth: true },
    },
    {
        path: '/order/photo',
        name: 'Photo',
        component: ()=> import('../views/order/Photo.vue'),
    },
    {
        path: '/order/puzle',
        name: 'Puzle',
        component: ()=> import('../views/order/Puzle.vue'),
    },
    {
        path: '/order/cup',
        name: 'Cup',
        component: ()=> import('../views/order/Cup.vue'),
    },
    {
        path: '/order/canvas',
        name: 'Canvas',
        component: ()=> import('../views/order/Canvas.vue'),
    },
    {
        path: '/404',
        name: '404',
        component: ()=> import('../views/404.vue'),
    }
];

const router = createRouter({
    history,
    routes,
});

export default router;
