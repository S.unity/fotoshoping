import { createStore } from "vuex";

export default createStore({
  state: {
    user: {},
    profile: {},
    products: [],
    cart: [],
    shop: []
  },
  mutations: {
    addCartItem(state, item) {
      let arr1 = []
      arr1.push(item)
      state.cart = arr1.concat(state.cart.filter(({ product_id }) => !arr1.find(i => {
        console.log(i)
        if (i.product_id === product_id) {
          i.CartQuantity++
        } else {
          i.CartQuantity = 1;
          state.cart.push(i);
        }
      })))
      console.log(state.cart)
    },
    updateCartItem(state, updatedItem) {
      state.cart = state.cart.map((cartItem) => {
        if (cartItem.id === updatedItem.id) {
          return updatedItem;
        }

        return cartItem;
      });
    },
    removeCartItem(state, item) {
      state.cart = state.cart.filter((cartItem) => {
        return cartItem.id !== item.id;
      });
    }
  }
});
// export const store = reactive({
//   user: {},
//   profile: {},
//   shop: [],
//   basket: []
// })
// export let basket = reactive([])
